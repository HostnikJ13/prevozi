# README #

Repozitorij vsebuje aplikacijo za iskanje prevozov znotraj Slovenije glede na začetni in končni kraj ter datum. 
Podatke pridobiva s spletne strani [https://prevoz.org/](https://prevoz.org/).

Uporabnik program požene z datoteko *prevozi.py* nato pa vpiše začetni in končni kraj ter izbere datum.
Pri vnosu imen krajev so uporabniku na voljo predlogi, ki jih lahko vidi in izbere tako, 
da klikne na puščico desno od vnosnega polja ali pa pritisne tipko "dol".
Program izpiše rezultate iskanja, na katere lahko kliknemo, če želimo več informacij o izbranem prevozu.
Preden poženete program si namestite nestandardno knjižnico [Requests](http://docs.python-requests.org/en/latest/).

Za več informacij si preberite poročilo.

Jure Hostnik