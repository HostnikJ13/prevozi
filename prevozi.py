# -*- coding: utf-8 -*-

__author__ = 'Jure Hostnik'


import requests
from tkinter import *
from tkinter.ttk import Combobox#, Progressbar
#import datetime

# Najprej pridobimo datume, ki so na voljo. 
datumi_r = requests.get('https://prevoz.org/').text
datumi_seznam1 = re.findall('<option value="(\d{4}-\d{2}-\d{2}).*">(.*?)</option>', datumi_r)
datumi_seznam2 = [(d[1], d[0]) for d in datumi_seznam1]
datumi_slovar = dict(datumi_seznam2)
datumi = [d[0] for d in datumi_seznam2]


##def ni_prepozno(cas):
##    """Funkcija, ki vrne True, če cas še ni prišel in False sicer."""
##    trenutni = str(datetime.datetime.now())
##    return trenutni <= cas


class Prevozi():
    def __init__(self, master):
        
        master.title('Prevozi')
        master.configure(bg='#F0FFFF')

        #self.pb = Progressbar(master, mode='indeterminate')
        #self.pb.grid(column=0, row=12, columnspan=9)

        p_od = (master.register(self.predlogi_od), '%P')
        p_do = (master.register(self.predlogi_do), '%P')
        
        Label(master, text='Kraj odhoda:', bg='#F0FFFF').grid(column=0, row=0)
        self.od = StringVar(master)
        self.Od = Combobox(master, width=24, textvariable=self.od,
                           validate='all', validatecommand=p_od)#, style='TEntry')
        self.Od.grid(column=0, row=1)
        self.Od.focus()

        Label(master, text='Kraj prihoda:', bg='#F0FFFF').grid(column=2, row=0)
        self.do = StringVar(master)
        self.Do = Combobox(master, width=24, textvariable=self.do,
                           validate='all', validatecommand=p_do)#, style='TEntry')
        self.Do.grid(column=2, row=1)

        Label(master, text='Kdaj:', bg='#F0FFFF').grid(column=3, row=0)
        self.dan = StringVar(master)
        kdaj = Combobox(master, textvariable=self.dan, state='readonly')
        kdaj.grid(column=3, row=1)
        kdaj['values'] = datumi
        kdaj.current(0)

        self.tocno = StringVar(master)
        Tocno = Checkbutton(master, text='samo točni zadetki', bg='#F0FFFF',
                            variable=self.tocno, onvalue='true', offvalue='false',
                            command=None)
        Tocno.grid(column=5, row=1)
        Tocno.deselect()

        self.isci = Button(master, text="IŠČI", command=self.main)
        self.isci.grid(column=8, row=1)
        Button(master, text="<-->", command=self.zamenjaj, takefocus=False).grid(column=1, row=1)

        Label(master, bg='#F0FFFF').grid(column=0, row=2, columnspan=9)

        napis = Label(master, text=' OD'+23*' '+'DO'+24*' '+'ČAS'+21*' '+'CENA(€)'+3*' ',
                      bg='#F0FFFF', font=('Courier New', 10, 'bold'))
        napis.grid(column=0, row=3, columnspan=9)

        sbL = Scrollbar(master)
        sbL.grid(column=9, row=4,  sticky=(N, S))
        self.rezultati = Listbox(master, height=10, font=('Courier New', 8), takefocus=False,
                                 selectmode=SINGLE, activestyle='none', yscrollcommand=sbL.set)
        self.rezultati.grid(column=0, row=4, columnspan=9, sticky=(E, W))
        sbL.config(command=self.rezultati.yview)
        self.rezultati.config(state=DISABLED)
        self.rezultati.bind('<Button-1>', self.pomozna)

        self.stevilo_zadetkov = StringVar(master)
        sz = Label(master, textvariable=self.stevilo_zadetkov, bg='#F0FFFF', justify=LEFT)
        sz.grid(column=0, row=5, sticky=(E, W))

        info = Label(master, text='Dodatne informacije o izbranem prevozu:',
                     bg='#F0FFFF', font=('Courier New', 10, 'bold'))
        info.grid(column=0, row=6, columnspan=9)

        sbT = Scrollbar(master)
        sbT.grid(column=9, row=7,  sticky=(N, S))
        self.tekst = Text(master, height=10, wrap=WORD, yscrollcommand=sbT.set)
        self.tekst.grid(column=0, row=7, columnspan=9, sticky=(E, W))
        sbT.config(command=self.tekst.yview)
        self.tekst.config(state=DISABLED)

        master.bind("<Return>", self.isci_enter)

    def isci_enter(self, *argv):
        """Metoda, ki posredno pokliče metodo "main".
           Hkrati poskrbi, da je gumb "IŠČI" ob pritisku na Enter vdrt,
           tako kot če bi nanj kliknili z miško."""
        self.isci.config(relief=SUNKEN)
        root.after(10, lambda: self.isci.invoke())
        root.after(10, lambda: self.isci.config(relief=RAISED))

    def predlogi_od(self, vnos):
        """Metoda, ki pridobiva predloge za kraj odhoda, katerega ime vnašamo."""
        je_beseda = vnos.isalpha() or (' ' in vnos) or ('.' in vnos)
        if je_beseda:
            if vnos[0] in 'čšž':            # Spletna stran razlikuje male in
                vnos = vnos.capitalize()    # velike črke samo za č, š in ž.
            parametri = {'country': 'SI', 'query': vnos}
            r = requests.get('https://prevoz.org/api/geonames/lookup/', params=parametri)
            l = r.json(encoding='utf-8')
            self.Od['values'] = l.get("suggestions")
        elif vnos == '':
            self.Od['values'] = []
        #self.Od.event_generate('<Down>')
        return je_beseda or vnos == ''

    def predlogi_do(self, vnos):
        """Metoda, ki pridobiva predloge za kraj prihoda, katerega ime vnašamo."""
        je_beseda = vnos.isalpha() or (' ' in vnos) or ('.' in vnos)
        if je_beseda:
            if vnos[0] in 'čšž':            # Spletna stran razlikuje male in
                vnos = vnos.capitalize()    # velike črke samo za č, š in ž.
            parametri = {'country': 'SI', 'query': vnos}
            r = requests.get('https://prevoz.org/api/geonames/lookup/', params=parametri)
            l = r.json(encoding='utf-8')
            self.Do['values'] = l.get("suggestions")
        elif vnos == '':
            self.Do['values'] = []
        #self.Do.event_generate('<Down>')
        return je_beseda or vnos == ''

    def zamenjaj(self):
        """Metoda, ki zamenja začetni in končni kraj."""
        od = self.od.get()
        do = self.do.get()
        self.od.set(do)
        self.do.set(od)
        Od = self.Od['values']
        self.Od['values'] = self.Do['values']
        self.Do['values'] = Od

    def main(self, *argv):
        """Pridobiva rezultate za iskan prevoz in jih izpisuje."""
        #self.pb.start()          
        self.rezultati.config(state=NORMAL)
        self.tekst.config(state=NORMAL)
        self.rezultati.delete(0, self.rezultati.size())
        self.tekst.delete('0.0', END)
        self.tekst.config(state=DISABLED)
        parametri = {
            'f': '',
            'fc': 'SI',
            't': '',
            'tc': 'SI',
            'd': '',
            'exact': self.tocno.get(),
            'intl': 'false'}
        od = self.od.get()
        if od == '':
            pass
        elif od[0] in 'čšž':          # Spletna stran razlikuje male in
            od = od.capitalize()    # velike črke samo za č, š in ž.
        do = self.do.get()
        if do == '':
            pass
        elif do[0] in 'čšž':          # Spletna stran razlikuje male in
            do = do.capitalize()    # velike črke samo za č, š in ž.
        parametri['f'] = od
        parametri['t'] = do
        parametri['d'] = datumi_slovar.get(self.dan.get())
        r = requests.get('https://prevoz.org/api/search/shares/', params=parametri)
        l = r.json(encoding='utf-8')
        prevozi = []            # seznam seznamov oblike [od, do, datum, cena]
        self.dodatne_info = []  # seznam seznamov oblike [št. oseb, telefon, avtor, komentar]
        for i in l.get('carshare_list'):
            #cas = ' '.join(i.get('date_iso8601').split('+')[0].split('T'))
            #if ni_prepozno(cas):
            prevozi.append([i.get('from'),
                            i.get('to'),
                            i.get('date'),
                            str(i.get('price')) if i.get('price') is not None else 'ni podatka'])
            self.dodatne_info.append([i.get('num_people'),
                                      i.get('contact'),
                                      i.get('author'),
                                      i.get('comment')])
        for p in prevozi:
            nova_vrstica = ''
            for j in p[:-1]:
                nova_vrstica += ' ' + j + (28-len(j)) * ' '
            c = p[-1]
            if c[-1] == '0':
                c = c[:-2]
                c = (2-len(c))*' '+c
            if '.' in c:
                c = ' ' + c
            nova_vrstica += c + (10-len(c)) * ' '
            self.rezultati.insert(END, nova_vrstica)
        #self.pb.stop()
        self.stevilo_zadetkov.set('Število zadetkov: ' + str(len(prevozi)))
        self.rezultati.focus()

    def pomozna(self, *argv):
        """Poskrbi, da se metoda "vec_info" pokliče za klikom in ne istočasno."""
        root.after(10, self.vec_info)

    def vec_info(self):
        """Metoda, ki izpiše več informacij o izbranem prevozu."""
        try:    # Izognemo se napaki. če uporabnik klikne na prazen Listbox.
            i = int(self.rezultati.curselection()[0])
        except IndexError:
            return None
        self.tekst.config(state=NORMAL)
        self.tekst.delete('0.0', END)
        self.tekst.insert(END, 'število oseb', ('š_o',))
        self.tekst.insert(END, ': ' + str(int(self.dodatne_info[i][0])))
        self.tekst.tag_config('š_o', underline=1)
        self.tekst.insert(END, '\ntelefon', ('t',))
        self.tekst.insert(END, ': ' + self.dodatne_info[i][1])
        self.tekst.tag_config('t', underline=1)
        self.tekst.insert(END, '\navtor', ('a',))
        self.tekst.insert(END, ': ' + self.dodatne_info[i][2])
        self.tekst.tag_config('a', underline=1)
        self.tekst.insert(END, '\nkomentar', ('k',))
        self.tekst.insert(END, ':\n' + ''.join(self.dodatne_info[i][3].split('\r')))
        self.tekst.tag_config('k', underline=1) 
        self.tekst.config(state=DISABLED)


root = Tk()

aplikacija = Prevozi(root)

root.mainloop()
